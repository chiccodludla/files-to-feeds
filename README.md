## files-to-feeds

####Project to create Twitter-like feeds from text files

##### Project setup
* Install latest Java JRE and JDK (from version 1.8 )
* Install Apache Maven 3.3.9 or above 
* Install Git client
* Git clone this repo *`$git clone https://chiccodludla@bitbucket.org/chiccodludla/files-to-feeds.git`*
* Install latest version of IntelliJ and open the project


##### Run Project
*From project root folder, run
 
 `mvn spring-boot:run`