package com.coding.twitterfeeds;

import com.coding.twitterfeeds.service.FeedsProcessService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class TwitterFeedsApplication implements CommandLineRunner {
	private FeedsProcessService feedsProcessService;

	@Autowired
	public TwitterFeedsApplication(FeedsProcessService feedsProcessService){
		this.feedsProcessService = feedsProcessService;
	}

	public static void main(String [] args) {
        SpringApplication app = new SpringApplication(TwitterFeedsApplication.class);
        app.run(args);
	}

	@Override
	public void run(String [] strings) {
        System.out.println("\n \t \tTwitter feeds \n");

		String userFile = getFilename("users");
		String tweetFile = getFilename("tweets");
		System.out.println("\n");

		feedsProcessService.displayFeeds(userFile, tweetFile);
	}

	private String getFilename(String filetype){
		System.out.print("Please enter a valid " + filetype + " text file name e.g. file.txt: ");
		String filename = new Scanner(System.in).nextLine();

		if(StringUtils.isBlank(filename) || !filename.endsWith(".txt")){
			return getFilename(filetype);
		}
		else {
			return filename;
		}
	}
}
