package com.coding.twitterfeeds.dao;

import com.coding.twitterfeeds.model.Tweet;

import java.util.List;
import java.util.Set;
import java.util.TreeMap;

public interface FileReaderDao {
    List<Tweet> getTweets(String filename);
    TreeMap<String, Set<String>> getUsers(String filename);
}
