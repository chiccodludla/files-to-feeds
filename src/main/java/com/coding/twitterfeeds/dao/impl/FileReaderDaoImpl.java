package com.coding.twitterfeeds.dao.impl;

import com.coding.twitterfeeds.dao.FileReaderDao;
import com.coding.twitterfeeds.model.Tweet;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URL;
import java.util.*;

@Repository
@Qualifier(value = "fileReaderDao")
public class FileReaderDaoImpl implements FileReaderDao {

    @Override
    public List<Tweet> getTweets(String filename) {
        FileReader tweetFileReader = null;
        BufferedReader tweetBufferedReader = null;

        try {
            File file = getFile(filename);
            tweetFileReader = new FileReader(file);
            tweetBufferedReader = new BufferedReader(tweetFileReader);
            List<Tweet> tweetsList = new ArrayList<>();

            String currentLine;

            while (StringUtils.isNotBlank(currentLine = tweetBufferedReader.readLine())) {
                String [] tweet = currentLine.split("> ");
                if(tweet.length > 1){ //possibility of incorrectly formatted line, so the line is ommited
                    tweetsList.add(new Tweet(tweet[0], tweet[1]));
                }
            }

            return tweetsList;

        }catch (FileNotFoundException e){
            System.err.println(e.getMessage());
        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            releaseReaders(tweetFileReader, tweetBufferedReader);
        }
        return null;
    }

    /**
     *
     * @param filename
     * @return TreeMap
     */
    @Override
    public TreeMap<String, Set<String>> getUsers(String filename) {
        FileReader fileReader = null;
        BufferedReader bufferedReader = null;

        try {
            File file = getFile(filename);
            TreeMap<String, Set<String>> userFollowersMap = new TreeMap<>();
            fileReader = new FileReader(file);
            bufferedReader = new BufferedReader(fileReader);
            String uCurrentLine;

            while (StringUtils.isNotBlank(uCurrentLine = bufferedReader.readLine())) {
                String [] users = uCurrentLine.split(" follows ");
                if(users.length > 1) {
                    String user = users[0];
                    Set<String> followedList = new HashSet<>(Arrays.asList(users[1].split(", ")));
                    addFollow(user, followedList, userFollowersMap);
                }
            }

            return userFollowersMap;

        }catch (FileNotFoundException e){
            System.err.println(e.getMessage());
        }catch (Exception ex){
            ex.printStackTrace();
        } finally {
            releaseReaders(fileReader, bufferedReader);
        }
        return null;
    }

    private File getFile(String filename) throws FileNotFoundException {
        ClassLoader classLoader = getClass().getClassLoader();
        URL url = classLoader.getResource(filename);
        if(url == null) {
            throw new FileNotFoundException(filename + " was not found or cannot be accessed.");
        }
        return new File(url.getFile());
    }

    /**
     * Recursive method to get each user with her following list
     * @param user
     * @param followedList - the list of other users followed by the 'user'
     * @param userFollowersMap - sorted map of user with list of people s/he follows i.e. {user: [...followings]}
     */
    private void addFollow(String user, Set<String> followedList, TreeMap<String, Set<String>> userFollowersMap){
        if (!userFollowersMap.containsKey(user)) {
            userFollowersMap.put(user, followedList);
        }
        else if(userFollowersMap.containsKey(user)) {
            if (followedList.isEmpty()) {
                return;
            } else {
                userFollowersMap.get(user).addAll(followedList);
            }
        }
        //from each "followings" list, call addFollow. Assign new user an empty "followings" list
        followedList.forEach(followed -> addFollow(followed.trim(), new HashSet<>(), userFollowersMap));
    }

    /**
     * @param fileReader
     * @param bufferedReader
     */
    private void releaseReaders(FileReader fileReader, BufferedReader bufferedReader) {
        try {
            if (bufferedReader != null)
                bufferedReader.close();

            if (fileReader != null)
                fileReader.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
