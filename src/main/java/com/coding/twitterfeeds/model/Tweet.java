package com.coding.twitterfeeds.model;

public class Tweet {
    private String owner;
    private String message;

    public Tweet(String owner, String message) {
        this.setOwner(owner);
        this.setMessage(message);
    }

    public String getOwner() {
        return owner;
    }

    public Tweet setOwner(String owner) {
        this.owner = owner;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Tweet setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getTweetFeed(){
        return "@" + owner + ": " + message;
    }
}
