package com.coding.twitterfeeds.service;

public interface FeedsProcessService {
    void displayFeeds(String userFilename, String tweetsFilename);
}
