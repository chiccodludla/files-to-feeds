package com.coding.twitterfeeds.service.Impl;

import com.coding.twitterfeeds.dao.FileReaderDao;
import com.coding.twitterfeeds.model.Tweet;
import com.coding.twitterfeeds.service.FeedsProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Service
@Qualifier(value = "feedsProcessService")
public class FeedsProcessServiceImpl implements FeedsProcessService {

    @Autowired
    @Qualifier("fileReaderDao")
    private FileReaderDao fileReaderDao;

    @Override
    public void displayFeeds(String userFilename, String tweetsFilename) {
        try {
            TreeMap<String, Set<String>> users = fileReaderDao.getUsers(userFilename);
            List<Tweet> tweets = fileReaderDao.getTweets(tweetsFilename);

            if(users != null) {
                users.keySet().forEach(username -> {

                    List<Tweet> filteredTweets = tweets.stream().filter(tweet -> tweet.getOwner().equals(username)
                            || users.get(username).contains(tweet.getOwner())).collect(Collectors.toList());

                    System.out.println(username);
                    filteredTweets.forEach(tweet -> System.out.println(tweet.getTweetFeed()));
                });
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
