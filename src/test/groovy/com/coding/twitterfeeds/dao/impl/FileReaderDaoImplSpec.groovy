package com.coding.twitterfeeds.dao.impl

import com.coding.twitterfeeds.dao.FileReaderDao
import com.coding.twitterfeeds.model.Tweet
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Title

@Title("unit test for FileReaderDaoImpl")
@Subject(FileReaderDaoImpl)
class FileReaderDaoImplSpec extends Specification{
    FileReaderDao fileReaderDao = new FileReaderDaoImpl()

    def 'getTweets test: should return list of Tweets' () {
        given: 'tweet filename i.e. tweet.txt'
        String filename = 'tweet.txt'

        when: 'getTweets is called'
        List<Tweet> tweets = fileReaderDao.getTweets(filename)

        then: 'List of tweets should be returned'
        tweets
        tweets.size() == 3
        tweets.get(1).owner == 'Ward'
        tweets.get(2).message == 'Happy ending, so on and so forth.'
    }

    def 'getTweets test: should return null for invalid file name' () {
        given: 'tweet filename i.e. tweet.txt'
        String filename = 'tweets.txt'

        when: 'getTweets is called'
        List<Tweet> tweets = fileReaderDao.getTweets(filename)

        then: 'null should be returned'
        !tweets
    }

    def 'getUsers test: should return Map of Users' () {
        given: 'user filename i.e. tweet.txt'
        String filename = 'user.txt'

        when: 'getUsers is called'
        TreeMap<String, Set<String>> users = fileReaderDao.getUsers(filename)

        then: 'Map of users should be returned'
        users
        users.size() == 3
        users.get('Alan').size() == 1
        users.get('Martin').size() == 0
        users.get('Ward').size() == 2
    }

    def 'getUsers test: should return null for invalid file name' () {
        given: 'user filename i.e. tweet.txt'
        String filename = 'users.txt'

        when: 'getUsers is called'
        TreeMap<String, Set<String>> users = fileReaderDao.getUsers(filename)

        then: 'null should be returned'
        !users
    }
}
