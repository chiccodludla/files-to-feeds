package com.coding.twitterfeeds.service.Impl

import com.coding.twitterfeeds.dao.FileReaderDao
import com.coding.twitterfeeds.model.Tweet
import com.coding.twitterfeeds.service.FeedsProcessService
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Title

@Title("unit test for FeedsProcessServiceImpl")
@Subject(FeedsProcessServiceImpl)
class FeedsProcessServiceImplSpec extends Specification {
    FileReaderDao fileReaderDao = Mock()
    FeedsProcessService feedsProcessService = new FeedsProcessServiceImpl(fileReaderDao: fileReaderDao)

    TreeMap<String, Set<String>> expectedUsers = new TreeMap<String, Set<String>>(){
        {
            put("user01", ["user03", "user09"].toSet())
            put("user02", ["user01", "user09"].toSet())
        }
    }

    List<Tweet> expectedTweets = [new Tweet("user01", "message one"), new Tweet("user02", "message two")]

    def 'displayFeeds: should display feeds to console' () {
        given: 'user filename & tweet filename'
        String userFilename = 'user.txt'
        String tweetFile = 'tweet.txt'

        when: 'displayFeeds is called'
        feedsProcessService.displayFeeds(userFilename, tweetFile)

        then: 'getUsers of fileReaderDao should be called'
        1 * fileReaderDao.getUsers({ String expectedFile -> expectedFile.equals(userFilename)}) >> expectedUsers

        then: 'getTweets of fileReaderDao should be called'
        1 * fileReaderDao.getTweets({ String expectedFile -> expectedFile.equals(tweetFile)}) >> expectedTweets
    }

    def 'displayFeeds: should throw exception' () {
        given: 'user filename & tweet filename'
        String userFilename = 'user.txt'
        String tweetFile = 'tweet.txt'

        when: 'displayFeeds is called'
        feedsProcessService.displayFeeds(userFilename, tweetFile)

        then: 'getUsers of fileReaderDao should be called'
        1 * fileReaderDao.getUsers({ String expectedFile -> expectedFile.equals(userFilename)})

        then: 'getTweets of fileReaderDao should be called'
        1 * fileReaderDao.getTweets({ String expectedFile -> expectedFile.equals(tweetFile)})
    }
}
